#include <Wire.h>

#define FOUR_BIT_PORT_1 5
#define FOUR_BIT_PORT_2 4
#define FOUR_BIT_PORT_3 3
#define FOUR_BIT_PORT_4 2
#define CONTROL_BUTTON_PORT 7
#define CONTROL_LED 13
#define TEMPERATURE_THRESHOLD 170 // Limite de temperatura para ativar o LED

class Sensor
{
    public:
        Sensor(unsigned int address)
        {
            this->address = address;
        }

        void readData()
        {
            Wire.requestFrom(this->address, 2);

            unsigned int buffer = 0x0000;
            while (Wire.available()) {
                unsigned char c = Wire.read();
                buffer = (buffer << 8) | c;
            }

          	Serial.print("Sensor n.");
          	Serial.print(this->address);
          	Serial.print(": ");
            Serial.println(buffer);

            this->readValue = buffer;
        }

        unsigned int getAddress()
        {
            return this->address;
        }

        unsigned int getReadValue()
        {
            return this->readValue;
        }

    private:
        unsigned int address;
        unsigned int readValue;
};

Sensor **sensors;

unsigned int numberOfSensors = 0;

class Notifier
{
    public:
        Notifier(unsigned int ledPin, unsigned int treshold)
        {
            this->ledPin = ledPin;
            this->treshold = treshold;

            this->notifyStatus = false;
        }

        void run()
        {
            this->notifyStatus = false;

            for (unsigned int i = 0; i < numberOfSensors; i++) {
                sensors[i]->readData();

                if (sensors[i]->getReadValue() > this->treshold) {
                    this->notifyStatus = true;
                }
            }

            digitalWrite(this->ledPin, LOW);
            if (this->notifyStatus) {
                digitalWrite(this->ledPin, (millis() / 500) % 2);
            }

            delay(10);
        }

    private:
        unsigned int ledPin;
        unsigned int treshold;
        bool notifyStatus;
};

Notifier *notifier;

unsigned int readFourBitPort();
bool validateSensorAddresses();
void runErrorState();

void setup()
{
    pinMode(FOUR_BIT_PORT_1, INPUT);
    pinMode(FOUR_BIT_PORT_2, INPUT);
    pinMode(FOUR_BIT_PORT_3, INPUT);
    pinMode(FOUR_BIT_PORT_4, INPUT);
    pinMode(CONTROL_BUTTON_PORT, INPUT);
    
    pinMode(CONTROL_LED, OUTPUT);

    delay(50);

    Serial.begin(9600);

    while (!Serial) {
        delay(10);
    }
  
  	Serial.println("Waiting for number of sensors");
  	while (digitalRead(CONTROL_BUTTON_PORT) == LOW) {
        digitalWrite(CONTROL_LED, HIGH);
        delay(100);
        Serial.print(".");
      }
  	Serial.println(" ");
    digitalWrite(CONTROL_LED, LOW);

    numberOfSensors = readFourBitPort();
  
  	Serial.print("Number of Sensors: ");
    Serial.println(numberOfSensors);

    sensors = (Sensor **)malloc(sizeof(Sensor *) * numberOfSensors);

    for (unsigned int i = 0; i < numberOfSensors; i++) {
        delay(200);
      	Serial.print("Waiting for address for sensor number ");
      	Serial.println(i);
        while (digitalRead(CONTROL_BUTTON_PORT) == LOW) {
            digitalWrite(CONTROL_LED, HIGH);
            delay(100);
          	Serial.print(".");
        }
      	Serial.println(" ");
        digitalWrite(CONTROL_LED, LOW);

        unsigned int sensorAddress = readFourBitPort();
        sensors[i] = new Sensor(sensorAddress);
      	Serial.print("Address got: ");
      	Serial.println(sensorAddress);
    }

    if (!validateSensorAddresses()) {
        Serial.println("Duplicate sensor addresses detected");
        Serial.println("Please restart the device and ensure all sensor addresses are unique");
        runErrorState();
    }

    Wire.begin();

    notifier = (Notifier *)malloc(sizeof(Notifier));
    notifier = new Notifier(CONTROL_LED, TEMPERATURE_THRESHOLD);
}

void loop()
{
    notifier->run();
}

unsigned int readFourBitPort()
{
    return (digitalRead(FOUR_BIT_PORT_1) << 3) | (digitalRead(FOUR_BIT_PORT_2) << 2) | (digitalRead(FOUR_BIT_PORT_3) << 1) | digitalRead(FOUR_BIT_PORT_4);
}

/**
 * @return true if all sensor addresses are unique, false otherwise
 */
bool validateSensorAddresses()
{
    // Check for duplicate addresses
    for (unsigned int i = 0; i < numberOfSensors; i++) {
        for (unsigned int j = i + 1; j < numberOfSensors; j++) {
            if (sensors[i]->getAddress() == sensors[j]->getAddress()) {
                return false;
            }
        }
    }

    return true;
    
}

void runErrorState()
{
    while(true) {
        digitalWrite(CONTROL_LED, HIGH);
        delay(500);
        digitalWrite(CONTROL_LED, LOW);
        delay(500);
    }
}
