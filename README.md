# Baby Care
## Description
This is a partial requirement for the completion of the course Projeto Integrado at the Federal University of Rio de Janeiro. The project consists of a baby care system that allows parents to monitor their baby's health and well-being. The system is composed of an Arduino board, a temperature sensor, a heart beat sensor and a microphone to monitor the baby's room. The system also communicates with a mobile application through bluetooth, which allows parents to monitor their baby's health and well-being in real time.

## Team
- Erick Morais
- Isa
- Jomar Junior

## How to run unit tests
```bash
$ cd baby-care
$ make test
```
It uses the Google Test framework to run the tests.