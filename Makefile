# Rule to run unit tests
test:
	bazel test --test_output=all --cxxopt=-std=c++14 //:unit_tests

test_debug:
	bazel test --test_output=all --cxxopt=-std=c++14 //:unit_tests_debug