#ifndef EEPROM_DATA_STORAGE_H
#define EEPROM_DATA_STORAGE_H
#ifndef TESTING

#include <Arduino.h>
#include "DataStorage.h"

#include "Types.h"

namespace BabyCare {
    namespace Infrastructure {
        class EEPROMDataStorage : public Core::DataStorage {
            private:
                uint16_t lastAddress = 0;
            public:
                EEPROMDataStorage();
                ~EEPROMDataStorage() = default;

                Type::ErrorType setup();
                bool hasData(uint16_t address) override;

                uint16_t read(uint16_t address) override;

                Type::ErrorType write(uint16_t address, uint16_t data) override;
        };
    }
}


#endif
#endif