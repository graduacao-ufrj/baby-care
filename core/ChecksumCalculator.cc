#ifndef TESTING
#include <Arduino.h>
#endif

#include "ChecksumCalculator.h"

#include "Types.h"
#ifdef TESTING
#include <cstdint>
#endif

namespace BabyCare {
    namespace Core {
        uint16_t ChecksumCalculator::calculate(uint16_t data, uint16_t modulo) {
            return sumBits(data, 14) % modulo;
        }

        uint16_t ChecksumCalculator::calculate(uint16_t data1, Type::SensorType sensorType, uint16_t modulo) {
            return (sumBits(data1, 10) + sumBits(sensorType, 4)) % modulo;
        }

        uint16_t ChecksumCalculator::sumBits(uint16_t data, uint8_t bits) {
            uint16_t checksum = 0;
            for (uint16_t i = 0; i < bits; i++) {
                checksum += (data >> i) & 0x01;
            }
            return checksum;
        }
    }
}
