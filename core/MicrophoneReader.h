/**
 * This file is part of the infrastructure layer.
 * This can not be tested as it is dependent on the Arduino in the real world.
 */

#ifndef MICROPHONE_READER_H
#define MICROPHONE_READER_H
#ifndef TESTING

#include <Arduino.h>

#include "SensorReader.h"
#include "Types.h"

namespace BabyCare {
    namespace Infrastructure {
        class MicrophoneReader : public Core::SensorReader {
            private:
                const Type::SensorType sensorType = Type::SensorType::microphone;
                uint8_t pin;
                bool isDigitalPin = false;
            public:
                MicrophoneReader(uint8_t pin);
                ~MicrophoneReader() = default;
                Type::ErrorType setup() override;
                uint16_t read() override;

                uint8_t getPin() const override;
        };
    }
}

#endif
#endif