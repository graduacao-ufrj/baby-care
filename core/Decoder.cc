#include "Decoder.h"

#ifndef TESTING
#include <Arduino.h>
#include <ArduinoSTL.h>
#endif

#include "Types.h"
#include "ChecksumCalculator.h"

#ifdef TESTING
#include <cstdint>
#endif

#include <map>

namespace BabyCare {
    namespace Core {
        #ifdef TESTING
        using namespace std;
        #endif

        std::map<std::string, uint16_t> Decoder::decode(uint16_t encodedData)
        {
            uint16_t checksum = (encodedData >> 14) & 0x0003;
            uint16_t sensorType = (encodedData >> 10) & 0x000F;
            uint16_t data = encodedData & 0x03FF;

            return {
                {"checksum", checksum},
                {"sensorType", sensorType},
                {"data", data}
            };
        }
    }
}
