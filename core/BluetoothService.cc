#include "BluetoothService.h"

#include <Arduino.h>
#include <SoftwareSerial.h>
#include <ArduinoSTL.h>

#include "Types.h"

namespace BabyCare {
    namespace Infrastructure {
        BluetoothService::BluetoothService(uint8_t rxPin, uint8_t txPin) {
            this->bluetooth = new SoftwareSerial(rxPin, txPin);
        }

        Type::ErrorType BluetoothService::setup() {
            bluetooth->begin(9600);
            Serial.println("Bluetooth setup");
            return Type::ErrorType::ok;
        }

        Type::ErrorType BluetoothService::startAlarm() {
            bluetooth->println("Alarme ativado!");
            // Serial.println("Alarme ativado!");
            return Type::ErrorType::ok;
        }

        Type::ErrorType BluetoothService::stopAlarm() {
            bluetooth->println("Fim do alarme! Alarme desativado.");
            // Serial.println("Fim do alarme! Alarme desativado.");
            return Type::ErrorType::ok;
        }
    }
}