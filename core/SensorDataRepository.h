#ifndef SENSOR_DATA_REPOSITORY_H
#define SENSOR_DATA_REPOSITORY_H

#ifndef TESTING
#include <Arduino.h>
#include <ArduinoSTL.h>
#endif

#include "DataStorage.h"
#include "Types.h"

#ifdef TESTING
#include <cstdint>
#endif

#include <map>

namespace BabyCare
{
    namespace Core
    {
        class SensorDataRepository {
        public:
            /**
             * Construct a new SensorDataRepository object.
             * 
             * @param dataStorage The data storage to use.
             */
            SensorDataRepository(DataStorage* dataStorage);
            ~SensorDataRepository();

            Type::ErrorType setup();

            /**
             * Get the all data registered for a sensor ordered from the newest to the oldest.
             * 
             * @param sensorType The sensor type.
             * @return The data registered for the sensor.
             */
            std::map<uint16_t, uint16_t> getSensorData(Type::SensorType sensorType);

            /**
             * Register a new data for a sensor.
             * 
             * @param sensorType The sensor type.
             * @param data The data read from the sensor to register.
             */
            Type::ErrorType registerSensorData(Type::SensorType sensorType, uint16_t data);

            /**
             * Register a new data for a sensor.
             * 
             * @param formatedData The data already formated to be stored. Checksum will be evaluated.
             */
            Type::ErrorType registerSensorData(uint16_t formatedData);

        private:
            DataStorage *dataStorage;
            std::map<
                Type::SensorType,
                std::map<uint16_t, uint16_t>
            > sensorsData;
        };
    }
}

#endif