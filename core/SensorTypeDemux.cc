#ifndef TESTING
#include <Arduino.h>
#include <ArduinoSTL.h>
#endif

#include "SensorTypeDemux.h"

#include "Types.h"

#include "ChecksumCalculator.h"

#ifdef TESTING
#include <cstdint>
#endif

#include <map>

#ifdef DEBUG
    #include <iostream>
    #define DEBUG_PRINT(x) std::cout << x << std::endl;
#else
    #define DEBUG_PRINT(x)
#endif

namespace BabyCare {
    namespace Core {
        #ifdef TESTING
        using namespace std;
        #endif

        std::map<Type::SensorType, uint16_t>
        SensorTypeDemux::demux(uint16_t data)
        {
            DEBUG_PRINT("\tSensorTypeDemux::demux");
            DEBUG_PRINT("\tData (binary): " << std::bitset<16>(data));

            // Data: [ b1 b2 ] [ b3 b4 b5 b6 ] [ b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 ]
            // b1 b2: Validation
            // b3 b4 b5 b6: Sensor type
            // b7 b8 b9 b10 b11 b12 b13 b14 b15 b16: Data
            // msb: b1, lsb: b16
            std::map<Type::SensorType, uint16_t> result;

            // Extract the validation
            // 0b1100 0000 0000 0000 = 0xC000
            // 0b0000 0011 = 0x03
            uint8_t validation = ((data & 0xC000) >> 14) & 0x03;
            DEBUG_PRINT("\tValidation (binary): " << std::bitset<2>(validation));

            // Check if the validation is correct using checksum modulo 4 on each bit except for the validation bits
            uint8_t checksum = ChecksumCalculator::calculate(data & 0x3FFF, 4);
            DEBUG_PRINT("\tChecksum (binary): " << std::bitset<8>(checksum));

            if (checksum % 4 != validation) {
                DEBUG_PRINT("\tValidation failed: " << std::bitset<8>(checksum) << " % 4 != " << std::bitset<2>(validation));
                #ifdef TESTING
                throw std::runtime_error("Invalid checksum");
                #else
                return result;
                #endif
            }

            // Extract the sensor type
            // 0b0011 1100 0000 0000 = 0x3C00
            // 0b0000 1111 = 0x00F
            uint8_t sensorType = ((data & 0x3C00) >> 10) & 0x0F;
            DEBUG_PRINT("\tSensor type (binary): " << std::bitset<4>(sensorType));
            if (sensorType >= static_cast<uint8_t>(Type::SensorType::sensorAmount)) {
                DEBUG_PRINT("\tInvalid sensor type: " << sensorType);
                #ifdef TESTING
                throw std::runtime_error("Invalid sensor type");
                #else
                return result;
                #endif
            }

            // Extract the data
            // 0b0000 0011 1111 1111 = 0x03FF
            uint16_t sensorData = data & 0x03FF;
            DEBUG_PRINT("\tSensor data (binary): " << std::bitset<10>(sensorData));

            // Map the sensor type and the data
            result[static_cast<Type::SensorType>(sensorType)] = sensorData;

            return result;
        }
    }
}