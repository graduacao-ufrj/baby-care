#ifndef SENSOR_TYPE_DEMUX_H
#define SENSOR_TYPE_DEMUX_H

#ifndef TESTING
#include <Arduino.h>
#include <ArduinoSTL.h>
#endif

#include "Types.h"

#ifdef TESTING
#include <cstdint>
#endif

#include <map>

namespace BabyCare {
    namespace Core {
        #ifdef TESTING
        using namespace std;
        #endif

        class SensorTypeDemux {
            public:
                /**
                 * This function disassembles the data into information about the sensor type and the data it contains.
                 * @param data The data to be disassembled. It is assumed that the data is in the format: [2bit:validation][4bit:sensor type][10bit:data]
                 * @return A map containing the sensor type and the data it contains. The map is in the format: [sensor type:data, value]
                 */
                static std::map<Type::SensorType, uint16_t> demux(uint16_t data);
        };
    }
}

#endif