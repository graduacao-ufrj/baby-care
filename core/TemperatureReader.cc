/**
 * This file is part of the infrastructure layer.
 * This can not be tested as it is dependent on the Arduino in the real world.
 */

#ifndef TESTING
#include <Arduino.h> 

#include "TemperatureReader.h"

#include "ChecksumCalculator.h"
#include "Encoder.h"
#include "Types.h"

namespace BabyCare {
    namespace Infrastructure {
        TemperatureReader::TemperatureReader(uint8_t pin) : pin(pin) {}

        Type::ErrorType TemperatureReader::setup() {
            pinMode(this->pin, INPUT);
            Serial.println("TemperatureReader setup");
            return Type::ErrorType::ok;
        }

        uint16_t TemperatureReader::read() {
            uint16_t reading = analogRead(A1);

            for (uint8_t i = 1; i < 100; i++) {
              reading += analogRead(A1);
            }

            float voltage = (reading / 100) * 5.0 / 1023.0;
            float temperature = voltage * 100;

            return static_cast<uint16_t>(temperature * 100);
        }

        uint8_t TemperatureReader::getPin() const {
            return this->pin;
        }
    }
}
#endif