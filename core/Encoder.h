#ifndef ENCODER_H
#define ENCODER_H

#ifndef TESTING
#include <Arduino.h>
#endif

#include "Types.h"

#ifdef TESTING
#include <cstdint>
#endif

namespace BabyCare {
    namespace Core {
        #ifdef TESTING
        using namespace std;
        #endif

        class Encoder {
            public:
                /**
                 * @brief Encodes the data with the given checksum and sensor type
                 * Make sure that checksum is calculated using modulo 4 so it can be encoded
                 */
                static uint16_t encode(uint16_t checksum, Type::SensorType sensorType, uint16_t data);
        };
    }
}

#endif