#ifndef TESTING
#include <Arduino.h>
#include <ArduinoSTL.h>
#endif

#include "SensorDataRepository.h"

#include "DataStorage.h"
#include "Types.h"
#include "ChecksumCalculator.h"

#ifdef TESTING
#include <cstdint>
#endif

#ifdef DEBUG
#include <iostream>
#define DEBUG_PRINT(x) std::cout << x << std::endl;
#else
#define DEBUG_PRINT(x)
#endif

#include <map>

namespace BabyCare {
    namespace Core {
        SensorDataRepository::SensorDataRepository(DataStorage* dataStorage): dataStorage(dataStorage) {}

        Type::ErrorType SensorDataRepository::setup() {
            this->dataStorage->setup();
            
            DEBUG_PRINT("SensorDataRepository::SensorDataRepository");
            /**
             * On initialization, the repository reads all the data from the data storage.
             * Arduino UNO has 1024 bytes of EEPROM memory.
             * Each data is 16 bytes long:
             *      The first 2 bytes are the checksum modulo 4 validation of the data.
             *      The next 4 bytes are the sensor type.
             *      The next 10 bytes are the sensor data.
             * Therefore, we can store **64** sensor readings, regardless of the sensor type.
             * The data is stored in the EEPROM one after the other in the sequence they were registered.
             * 
             * The dataStorag->read() method returns the data stored in the EEPROM in the following range:
             * [address * 16, (address + 1) * 16) .
             */
            for (uint16_t i = 0; i < 64; i++) {
                if (dataStorage->hasData(i)) {
                    DEBUG_PRINT("\tReading data from address " << i);
                    uint16_t data = dataStorage->read(i);
                    DEBUG_PRINT("\tData (binary): " << std::bitset<16>(data));

                    uint16_t checksum = ((data & 0b1100000000000000) >> 14) & 0b0000000000000011;
                    DEBUG_PRINT("\tChecksum (binary): " << std::bitset<2>(checksum));
                    Type::SensorType sensorType = static_cast<Type::SensorType>(((data & 0b00111100000000) >> 10) & 0b0000000000001111);
                    DEBUG_PRINT("\tSensor type (binary): " << std::bitset<4>(sensorType));
                    uint16_t sensorData = data & 0b0000001111111111;
                    DEBUG_PRINT("\tSensor data (binary): " << std::bitset<10>(sensorData));

                    uint16_t checksumValidation = ChecksumCalculator::calculate(data, sensorType, 4);

                    if (!(checksum == checksumValidation)) {
                        DEBUG_PRINT("\tChecksum validation failed for data at address " << i);

                        DEBUG_PRINT("\tChecksum validation (binary): " << std::bitset<2>(checksumValidation));
                        continue;
                    }

                    size_t sensorDataSize = this->sensorsData[(Type::SensorType)sensorType].size();

                    // This is to avoid freezing the system when the data is too big
                    // (Arduino UNO is too weak, too weak)
                    if (sensorDataSize >= MAX_SENSOR_READS) {
                        this->sensorsData[(Type::SensorType)sensorType] = std::map<uint16_t, uint16_t>();
                        sensorDataSize = 0;
                    }
                    this->sensorsData[(Type::SensorType)sensorType][sensorDataSize] = sensorData;
                    DEBUG_PRINT("\tsensorsData[" << sensorType << "][" << sensorDataSize << "] = " << sensorData);
                }
            }

            return Type::ErrorType::ok;
        }

        SensorDataRepository::~SensorDataRepository() {
            DEBUG_PRINT("SensorDataRepository::~SensorDataRepository");
            delete dataStorage;
        }

        std::map<uint16_t, uint16_t> SensorDataRepository::getSensorData(Type::SensorType sensorType) {
            DEBUG_PRINT("SensorDataRepository::getSensorData");
            DEBUG_PRINT("\tRepository size: " << sensorsData.size());
            DEBUG_PRINT("\tReturning data for sensor type " << sensorType);
            DEBUG_PRINT("\tData size: " << sensorsData[sensorType].size());
            return sensorsData[sensorType];
        }

        Type::ErrorType SensorDataRepository::registerSensorData(Type::SensorType sensorType, uint16_t data) {
            DEBUG_PRINT("SensorDataRepository::registerSensorData");
            uint16_t checksum = ChecksumCalculator::calculate(data, sensorType, 4);

            uint16_t formattedData = (checksum << 14) | (sensorType << 10) | data;

            uint16_t address = 0;
            while (dataStorage->hasData(address)) {
                address++;
            }

            DEBUG_PRINT("\tWriting data to address " << address);
            DEBUG_PRINT("\tFormatted Data (binary): " << std::bitset<16>(formattedData));
    
            size_t sensorDataSize = this->sensorsData[sensorType].size();

            if (sensorDataSize >= MAX_SENSOR_READS) {
                this->sensorsData[sensorType] = std::map<uint16_t, uint16_t>();
                sensorDataSize = 0;
            }

            this->sensorsData[sensorType][sensorDataSize] = data;

            DEBUG_PRINT("\tsensorsData[" << sensorType << "][" << sensorDataSize << "] = " << data);
            return dataStorage->write(address, formattedData);
        }

        Type::ErrorType SensorDataRepository::registerSensorData(uint16_t formatedData) {
            DEBUG_PRINT("SensorDataRepository::registerSensorData");
            uint16_t checksum = ((formatedData & 0b1100000000000000) >> 14) & 0b0000000000000011;
            Type::SensorType sensorType = static_cast<Type::SensorType>(((formatedData & 0b00111100000000) >> 10) & 0b0000000000001111);
            uint16_t sensorData = formatedData & 0b0000001111111111;

            uint16_t checksumValidation = ChecksumCalculator::calculate(formatedData, sensorType, 4);

            if (!(checksum == checksumValidation)) {
                return Type::ErrorType::wrongChecksum;
            }


            uint16_t address = 0;
            while (dataStorage->hasData(address)) {
                address++;
            }

            DEBUG_PRINT("\tWriting data to address " << address);
            DEBUG_PRINT("\tData (binary): " << std::bitset<16>(formatedData));
            size_t sensorDataSize = this->sensorsData[sensorType].size();
            this->sensorsData[sensorType][sensorDataSize] = sensorData;
            return dataStorage->write(address, formatedData);
        }
    }
}