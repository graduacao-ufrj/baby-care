/**
 * This file is part of the infrastructure layer.
 * This can not be tested as it is dependent on the Arduino in the real world.
 */

#ifndef HEARTBEAT_READER_H
#define HEARTBEAT_READER_H
#ifndef TESTING
#include <Arduino.h>

#include "SensorReader.h"
#include "Types.h"

namespace BabyCare {
    namespace Infrastructure {
        class HeartbeatReader : public Core::SensorReader {
            private:
                const Type::SensorType sensorType = Type::SensorType::heartbeat;
                uint8_t pin;
                uint16_t reading;
                uint16_t threshold;
                bool isDigitalPin = false;
            public:
                HeartbeatReader(uint8_t pin, uint16_t treshold);
                ~HeartbeatReader() = default;
                Type::ErrorType setup() override;
                uint16_t read() override;

                uint8_t getPin() const override;
        };
    }
}

#endif
#endif