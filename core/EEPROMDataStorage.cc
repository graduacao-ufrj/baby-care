#ifndef TESTING

#include "EEPROMDataStorage.h"

#include <Arduino.h>

#include <EEPROM.h>
#include "Types.h"

namespace BabyCare {
    namespace Infrastructure {
        EEPROMDataStorage::EEPROMDataStorage() {}

        Type::ErrorType EEPROMDataStorage::setup() {
            return Type::ErrorType::ok;
        }

        bool
        EEPROMDataStorage::hasData(uint16_t address)
        {
            uint16_t data = this->read(address);

            return data != 0xFFFF;
        }

        /**
         * Read data from the EEPROM.
         * The first address is reserved for the last address written to.
         * It is not possible to use this method to read the last address written to.
         * If the address list is sequential, the last address written is the last unavailable address.
         */
        uint16_t
        EEPROMDataStorage::read(uint16_t address) 
        {
            uint8_t lowByte = EEPROM.read(address * 2);
            uint8_t highByte = EEPROM.read(address * 2 + 1);
            return ((highByte << 8) & 0xFF00) | (lowByte & 0x00FF);
        }

        Type::ErrorType
        EEPROMDataStorage::write(uint16_t address, uint16_t data)
        {
            /**
             * Use update instead of write to avoid writing the same data to the EEPROM.
             * This is to prevent wearing out the EEPROM.
             */
            if (address == MAX_EEPROM_ADDRESS) {
                for (int i = 0; i < EEPROM.length(); i++) {
                    EEPROM.update(i, 0xFF);
                }
            }

            EEPROM.update(address * 2, static_cast<uint8_t>(data & 0x00FF));
            EEPROM.update(address * 2 + 1, static_cast<uint8_t>((data >> 8) & 0x00FF));

            return Type::ErrorType::ok;
        }
    }
}

#endif