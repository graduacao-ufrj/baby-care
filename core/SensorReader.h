#ifndef SENSOR_READER_H
#define SENSOR_READER_H

#ifndef TESTING
#include <Arduino.h>
#endif

#ifdef TESTING
#include <cstdint>
#endif

#include "Types.h"

namespace BabyCare {
    namespace Core {
        #ifdef TESTING
        using namespace std;
        #endif

        class SensorReader {
            private:
                uint8_t pin;
            public:
                virtual Type::ErrorType setup() = 0;
                virtual uint16_t read() = 0;
                virtual uint8_t getPin() const = 0;
        };
    }
}

#endif