/**
 * This file is part of the infrastructure layer.
 * This can not be tested as it is dependent on the Arduino in the real world.
 */

#ifndef TEMPERATURE_READER_H
#define TEMPERATURE_READER_H
#ifndef TESTING

#include <Arduino.h>

#include "SensorReader.h"
#include "Types.h"

namespace BabyCare {
    namespace Infrastructure {
        class TemperatureReader : public Core::SensorReader {
            private:
                const Type::SensorType sensorType = Type::SensorType::temperature;
                uint8_t pin;
                bool isDigitalPin = false;
            public:
                TemperatureReader(uint8_t pin);
                ~TemperatureReader() = default;
                Type::ErrorType setup() override;
                uint16_t read() override;

                uint8_t getPin() const override;
        };
    }
}

#endif
#endif