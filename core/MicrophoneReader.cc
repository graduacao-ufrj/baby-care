/**
 * This file is part of the infrastructure layer.
 * This can not be tested as it is dependent on the Arduino in the real world.
 */

#ifndef TESTING
#include <Arduino.h>

#include "MicrophoneReader.h"

#include "ChecksumCalculator.h"
#include "Encoder.h"
#include "Types.h"

namespace BabyCare {
    namespace Infrastructure {
        MicrophoneReader::MicrophoneReader(uint8_t pin) : pin(pin) {}

        Type::ErrorType MicrophoneReader::setup() {
            pinMode(this->pin, INPUT);
            Serial.println("MicrophoneReader setup");
            return Type::ErrorType::ok;
        }

        uint16_t MicrophoneReader::read() {
            uint32_t startMillis = millis();
            uint16_t peakToPeak = 0;

            uint16_t signalMax = 0;
            uint16_t signalMin = 1024;

            while (millis() - startMillis < 10) {
                uint16_t sample = analogRead(this->pin);
                if (sample < 1024) {
                    if (sample > signalMax) {
                        signalMax = sample;
                    } else if (sample < signalMin) {
                        signalMin = sample;
                    }
                }
            }

            peakToPeak = signalMax - signalMin;
            return peakToPeak;
        }

        uint8_t MicrophoneReader::getPin() const {
            return this->pin;
        }
    }
}
#endif