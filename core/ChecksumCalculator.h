#ifndef CHECKSUM_CALCULATOR_H
#define CHECKSUM_CALCULATOR_H

#ifndef TESTING
#include <Arduino.h>
#endif

#include "Types.h"

#ifdef TESTING
#include <cstdint>
#endif

namespace BabyCare {
    namespace Core {
        class ChecksumCalculator {
        private:
            static uint16_t sumBits(uint16_t data, uint8_t bits);
        public:
            static uint16_t calculate(uint16_t data, uint16_t modulo);
            static uint16_t calculate(uint16_t data, Type::SensorType sensorType, uint16_t modulo);
        };
    }
}

#endif