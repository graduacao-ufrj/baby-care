#ifndef DATA_STORAGE_H
#define DATA_STORAGE_H

#ifndef TESTING
#include <Arduino.h>
#endif

#include "Types.h"

#ifdef TESTING
#include <cstdint>
#endif

/**
 * Abstraction of an EEPROM data storage of the ARDUINO UNO board.
 */
namespace BabyCare
{
    namespace Core {
        #ifdef TESTING
            using namespace std;
        #endif
        class DataStorage {
        public:
            virtual ~DataStorage() = default;
            virtual Type::ErrorType setup() = 0;

            /**
             * Checks if there is data in the specified address.
             * 
             * @param address The address to check.
             * @return True if there is data in the address, false otherwise.
             */
            virtual bool hasData(uint16_t address) = 0;

            /**
             * Read data from the EEPROM.
             * 
             * @param address The address to read from.
             * @return The data read from the EEPROM.
             */
            virtual uint16_t read(uint16_t address) = 0;

            /**
             * Write data to the EEPROM.
             * 
             * @param address The address to write to.
             * @param data The data to write.
             */
            virtual Type::ErrorType write(uint16_t address, uint16_t data) = 0;
        };
    }
}

#endif