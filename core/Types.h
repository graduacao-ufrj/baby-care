#ifndef TYPES_h
#define TYPES_h

#ifndef TESTING
#include <Arduino.h>
#endif

#define CHECKSUM_MODULO 4
#define BT_RX_PIN 2
#define BT_TX_PIN 3
#define MAX_SENSOR_READS 1
#define MAX_EEPROM_ADDRESS 59

#define HEARTBEAT_MEDIAN 80
#define HEARTBEAT_TRESHOLD 30

#define TEMPERATURE_MEDIAN 3500
#define TEMPERATURE_TRESHOLD 500

#define MICROPHONE_MEDIAN 200
#define MICROPHONE_TRESHOLD 400

namespace BabyCare {
    namespace Type {
        enum ErrorType {
            ok,
            wrongChecksum,
            EEPROMAddressOutOfRange,
            EEPROMNotSequential,
            errorAmount
        };

        enum SensorType {
            heartbeat,
            temperature,
            microphone,
            sensorAmount
        };
    }
}

#endif