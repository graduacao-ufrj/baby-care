#include "Encoder.h"

#ifndef TESTING
#include <Arduino.h>
#endif

#include "Types.h"

#ifdef TESTING
#include <cstdint>
#endif

namespace BabyCare {
    namespace Core {
        #ifdef TESTING
        using namespace std;
        #endif

        uint16_t
        Encoder::encode(uint16_t checksum, Type::SensorType sensorType, uint16_t data)
        {
            return ((checksum << 14) & 0xC000 ) | ((sensorType << 10) & 0x3C00) | (data & 0x03FF);
        }
    }
}
