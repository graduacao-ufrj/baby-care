#include <Arduino.h>
#include <ArduinoSTL.h>

#include "Types.h"

#include "HeartbeatReader.h"
#include "TemperatureReader.h"
#include "MicrophoneReader.h"

#include "BluetoothService.h"

#include <map>

using namespace BabyCare;

#define SIGNAL_LED_PIN 13

String formatBits(uint16_t value)
{
    String result = "";
    for (int i = 15; i >= 0; i--) {
        result += (value & (1 << i)) ? "1" : "0";
    }

    return result;
}

Infrastructure::HeartbeatReader heartbeatReader(A0, HEARTBEAT_TRESHOLD);
Infrastructure::TemperatureReader temperatureReader(A1);
Infrastructure::MicrophoneReader microphoneReader(A2);
Infrastructure::BluetoothService bluetoothService(BT_RX_PIN, BT_TX_PIN);

void setup()
{
    Serial.begin(115200);
    delay(100);
    Serial.println("BabyCare started");
    heartbeatReader.setup();
    temperatureReader.setup();
    microphoneReader.setup();
    bluetoothService.setup();

    pinMode(SIGNAL_LED_PIN, OUTPUT);
}

void loop()
{
    static uint32_t lastPrint = millis();
    const int printInterval = 1000;

    if (millis() - lastPrint > printInterval) {
        Serial.println("----------------------------------------------------------------");
        Serial.println("----------------------------------------------------------------");
    }

    std::map<Type::SensorType, uint16_t> readings = {
        {Type::SensorType::heartbeat, heartbeatReader.read()},
        {Type::SensorType::temperature, temperatureReader.read()},
        {Type::SensorType::microphone, microphoneReader.read()}
    };

    if (millis() - lastPrint > printInterval) {
        Serial.println("Readings:");
        for (const auto& reading : readings) {
            Serial.print("Sensor: ");
            Serial.print(reading.first);
            Serial.print(" Value: ");
            Serial.print(reading.second);
            Serial.println();
        }
    }

    bool isHeartbeatOk = readings[Type::SensorType::heartbeat] > (HEARTBEAT_MEDIAN - HEARTBEAT_TRESHOLD)
     && readings[Type::SensorType::heartbeat] < (HEARTBEAT_MEDIAN + HEARTBEAT_TRESHOLD);

    bool isTemperatureOk = readings[Type::SensorType::temperature] > (TEMPERATURE_MEDIAN - TEMPERATURE_TRESHOLD)
     && readings[Type::SensorType::temperature] < (TEMPERATURE_MEDIAN + TEMPERATURE_TRESHOLD);

    bool isMicrophoneOk = readings[Type::SensorType::microphone] < (MICROPHONE_MEDIAN + MICROPHONE_TRESHOLD);

    if (millis() - lastPrint > printInterval) {
        Serial.println((isHeartbeatOk ? "Heartbeat is ok" : "Heartbeat is not ok"));
        Serial.println((isTemperatureOk ? "Temperature is ok" : "Temperature is not ok"));
        Serial.println((isMicrophoneOk ? "Microphone is ok" : "Microphone is not ok"));
    }

    if (!isHeartbeatOk || !isTemperatureOk || !isMicrophoneOk) {
        if (millis() - lastPrint > printInterval) {
            Serial.println("Oh no! An alarm has been triggered!");
            Serial.println("Sending alarm signal to the bluetooth module");
        }
        //digitalWrite(SIGNAL_LED_PIN, HIGH);

        if (millis() - lastPrint > printInterval) {
            lastPrint = millis();
        }
        bluetoothService.startAlarm();
        return;
    }

    if (millis() - lastPrint > printInterval) {
        Serial.println("Thanks God! Everything is fine!");
        Serial.println("Sending stop alarm signal to the bluetooth module");
    }

    if (millis() - lastPrint > printInterval) {
        lastPrint = millis();
    }
    //digitalWrite(SIGNAL_LED_PIN, LOW);
    bluetoothService.stopAlarm();
}