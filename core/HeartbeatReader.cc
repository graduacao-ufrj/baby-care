/**
 * This file is part of the infrastructure layer.
 * This can not be tested as it is dependent on the Arduino in the real world.
 */

#ifndef TESTING
#include <Arduino.h>

#include "HeartbeatReader.h"

#include "ChecksumCalculator.h"
#include "Encoder.h"
#include "Types.h"

namespace BabyCare {
    namespace Infrastructure {
        HeartbeatReader::HeartbeatReader(uint8_t pin, uint16_t threshold) : pin(pin), threshold(threshold) {
            if (pin < 14 && pin < 21) {
                /** @todo Throw arduino level error */
            }
        }

        Type::ErrorType HeartbeatReader::setup() {
            pinMode(this->pin, INPUT);
            Serial.println("HeartbeatReader setup");
            return Type::ErrorType::ok;
        }

        uint16_t HeartbeatReader::read() {
            static int16_t lastReads[50] = {0};
            static int16_t lastReadIndex = 0;
            static int16_t lastTime = millis();
            static int16_t bpm = 0;
            static int16_t beats = 0;
            static int16_t interval = 10000; // 10 second interval

            int16_t reading = analogRead(this->pin);
            int16_t time = millis();

            lastReads[lastReadIndex] = reading;
            lastReadIndex = (lastReadIndex + 1) % 10;

            int16_t sum = 0;
            for (int16_t i = 0; i < 10; i++) {
                sum += lastReads[i];
            }

            int16_t peak = (reading - (sum/10));
            int16_t lastPeak = (lastReads[lastReadIndex] - (sum/10));

            if (peak >= 10) {
                if (lastPeak < 10) {
                    beats++;
                    digitalWrite(13, HIGH);
                }
            } else {
                digitalWrite(13, LOW);
            }

            if (time - lastTime >= interval) {
                bpm = beats * 3;
                beats = 0;
                lastTime = time;
            }

            return bpm;
        }

        uint8_t HeartbeatReader::getPin() const {
            return this->pin;
        }
    }
}

#endif