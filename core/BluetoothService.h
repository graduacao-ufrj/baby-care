#ifndef BLUETOOTH_SERVICE_H
#define BLUETOOTH_SERVICE_H

#include <Arduino.h>
#include <ArduinoSTL.h>
#include <SoftwareSerial.h>

#include "Types.h"

namespace BabyCare {
    namespace Infrastructure {
        class BluetoothService {
            public:
                BluetoothService() = default;
                BluetoothService(uint8_t rxPin, uint8_t txPin);
                ~BluetoothService() = default;
                Type::ErrorType setup();
                Type::ErrorType startAlarm();
                Type::ErrorType stopAlarm();

            private:
                SoftwareSerial *bluetooth;
        };
    }
}

#endif