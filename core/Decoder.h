#ifndef DECODER_H
#define DECODER_H

#ifndef TESTING
#include <Arduino.h>
#include <ArduinoSTL.h>
#endif

#include "Types.h"

#ifdef TESTING
#include <cstdint>
#endif

#include <map>

namespace BabyCare {
    namespace Core {
        #ifdef TESTING
        using namespace std;
        #endif

        class Decoder {
            public:
                /**
                 * @brief Encodes the data to be sent to the server
                 * @param encodedData The data to be encoded
                 * 
                 * @return The decoded data in the form of a map: {
                 *      "checksum": The checksum of the data,
                 *      "sensorType": The sensor type of the data,
                 *      "data": The data read from the sensor
                 * }
                 */
                static std::map<std::string, uint16_t> decode(uint16_t encodedData);
        };
    }
}

#endif