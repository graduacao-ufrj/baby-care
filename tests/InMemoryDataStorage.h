#ifndef IN_MEMORY_DATA_STORAGE_H
#define IN_MEMORY_DATA_STORAGE_H

#include "../core/DataStorage.h"

#include "../core/Types.h"

#include <map>
#include <cstdint>

namespace BabyCare {
    namespace Test {
        using namespace std;
        class InMemoryDataStorage: public Core::DataStorage {
        public:
            InMemoryDataStorage();
            ~InMemoryDataStorage() = default;
            bool hasData(uint16_t address) override;
            uint16_t read(uint16_t address) override;
            Type::ErrorType write(uint16_t address, uint16_t data) override;
        private:
            std::map<uint16_t, uint16_t> data;
        };
    }
}

#endif