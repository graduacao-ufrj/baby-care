#include "InMemoryDataStorage.h"

#include "../core/Types.h"

#include <map>
#include <cstdint>

#include <iostream>

#ifdef DEBUG
#define DEBUG_PRINT(x) std::cout << x << std::endl;
#else
#define DEBUG_PRINT(x)
#endif

namespace BabyCare {
    namespace Test {
        InMemoryDataStorage::InMemoryDataStorage() {
            this->data = std::map<uint16_t, uint16_t>();

            // Initialize the data storage with some data
            this->data[0] = 0b0100000000000000 | ((static_cast<uint16_t>(Type::SensorType::heartbeat) << 10) & 0b00111100000000) | 256;
            this->data[1] = 0b1000000000000000 | ((static_cast<uint16_t>(Type::SensorType::temperature) << 10) & 0b00111100000000) | 512;
            this->data[2] = 0b1100000000000000 | ((static_cast<uint16_t>(Type::SensorType::microphone) << 10) & 0b00111100000000) | 768;
            this->data[3] = 0b0100000000000000 | ((static_cast<uint16_t>(Type::SensorType::heartbeat) << 10) & 0b00111100000000) | 512;
            this->data[4] = 0b1100000000000000 | ((static_cast<uint16_t>(Type::SensorType::temperature) << 10) & 0b00111100000000) | 768;
        }

        bool InMemoryDataStorage::hasData(uint16_t address) {
            return this->data.size() > address;
        }

        uint16_t InMemoryDataStorage::read(uint16_t address) {
            DEBUG_PRINT("InMemoryDataStorage::read");
            DEBUG_PRINT("\tRepository size: " << this->data.size());
            DEBUG_PRINT("\tAddress: " << address);
            DEBUG_PRINT("\tData (binary): " << std::bitset<16>(this->data[address]));
            return this->data[address];
        }

        Type::ErrorType InMemoryDataStorage::write(uint16_t address, uint16_t data) {
            DEBUG_PRINT("InMemoryDataStorage::write");
            DEBUG_PRINT("\tOld repository size: " << this->data.size());
            DEBUG_PRINT("\tAddress: " << address);
            DEBUG_PRINT("\tData (binary): " << std::bitset<16>(data));
            this->data[address] = data;

            DEBUG_PRINT("\tNew repository size: " << this->data.size());
            return Type::ErrorType::ok;
        }
    }
}