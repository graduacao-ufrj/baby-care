#include "../core/MicrophoneEvaluator.h"

#include <gtest/gtest.h>

using namespace BabyCare;

TEST(MicrophoneEvaluatorTest, test_it_evaluates_false_when_last_reading_is_close_to_mean) {
   uint16_t mean = 365;
   uint16_t threshold = 5;

    Core::MicrophoneEvaluator evaluator(mean, threshold);

    std::map<uint16_t,uint16_t> readings = {
        {0, 365},
        {1, 365},
        {2, 365},
        {3, 365}
    };

    EXPECT_FALSE(evaluator.evaluate(readings));
}

TEST(MicrophoneEvaluatorTest, test_it_evaluates_true_when_last_reading_is_far_up_threshold_from_mean) {
   uint16_t mean = 365;
   uint16_t threshold = 5;

    Core::MicrophoneEvaluator evaluator(mean, threshold);

    std::map<uint16_t,uint16_t> readings = {
        {0, 370},
        {1, 365},
        {2, 365},
        {3, 375}
    };

    EXPECT_TRUE(evaluator.evaluate(readings));
}

TEST(MicrophoneEvaluatorTest, test_it_evaluates_true_when_last_reading_is_far_down_threshold_from_mean) {
   uint16_t mean = 365;
   uint16_t threshold = 5;

    Core::MicrophoneEvaluator evaluator(mean, threshold);

    std::map<uint16_t,uint16_t> readings = {
        {0, 360},
        {1, 365},
        {2, 365},
        {3, 355}
    };

    EXPECT_TRUE(evaluator.evaluate(readings));
}