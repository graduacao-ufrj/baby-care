#include "../core/HeartbeatEvaluator.h"

#include <gtest/gtest.h>

using namespace BabyCare;

TEST(HeartbeatEvaluatorTest, test_it_evaluates_false_when_last_reading_is_close_to_mean) {
   uint16_t mean = 10;
   uint16_t threshold = 5;

    Core::HeartbeatEvaluator evaluator(mean, threshold);

    std::map<uint16_t,uint16_t> readings = {
        {0, 10},
        {1, 10},
        {2, 10},
        {3, 10},
    };

    EXPECT_FALSE(evaluator.evaluate(readings));
}

TEST(HeartbeatEvaluatorTest, test_it_evaluates_true_when_last_reading_is_far_up_threshold_from_mean) {
   uint16_t mean = 10;
   uint16_t threshold = 5;

    Core::HeartbeatEvaluator evaluator(mean, threshold);

    std::map<uint16_t,uint16_t> readings = {
        {0, 15},
        {1, 10},
        {2, 10},
        {3, 20},
    };

    EXPECT_TRUE(evaluator.evaluate(readings));
}

TEST(HeartbeatEvaluatorTest, test_it_evaluates_true_when_last_reading_is_far_down_threshold_from_mean) {
   uint16_t mean = 10;
   uint16_t threshold = 5;

    Core::HeartbeatEvaluator evaluator(mean, threshold);

    std::map<uint16_t,uint16_t> readings = {
        {0, 5},
        {1, 10},
        {2, 10},
        {3, 0},
    };

    EXPECT_TRUE(evaluator.evaluate(readings));
}