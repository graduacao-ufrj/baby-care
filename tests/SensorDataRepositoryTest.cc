#include "../core/SensorDataRepository.h"

#include "InMemoryDataStorage.h"

#include <gtest/gtest.h>
#include <cstdint>
#include <map>

using namespace BabyCare;
TEST(SensorDataRepositoryTest, it_can_be_constructed_from_a_data_storage) {
    BabyCare::Test::InMemoryDataStorage *dataStorage = new BabyCare::Test::InMemoryDataStorage();
    try {
        Core::SensorDataRepository sensorDataRepository(dataStorage);
    } catch (...) {
        FAIL();
    }

    SUCCEED();
}

TEST(SensorDataRepositoryTest, it_can_get_sensor_data_of_heart_beat) {
    BabyCare::Test::InMemoryDataStorage *dataStorage = new BabyCare::Test::InMemoryDataStorage();
    Core::SensorDataRepository sensorDataRepository(dataStorage);

    std::map<uint16_t, uint16_t> sensorData = sensorDataRepository.getSensorData(Type::SensorType::heartbeat);

    ASSERT_EQ(sensorData.size(), 2);
    ASSERT_EQ(sensorData[0], 256);
    ASSERT_EQ(sensorData[1], 512);
}

TEST(SensorDataRepositoryTest, it_can_get_sensor_data_of_microphone) {
    BabyCare::Test::InMemoryDataStorage *dataStorage = new BabyCare::Test::InMemoryDataStorage();
    Core::SensorDataRepository sensorDataRepository(dataStorage);

    std::map<uint16_t, uint16_t> sensorData = sensorDataRepository.getSensorData(Type::SensorType::microphone);

    ASSERT_EQ(sensorData.size(), 1);
    ASSERT_EQ(sensorData[0], 768);
}

TEST(SensorDataRepositoryTest, it_can_get_sensor_data_of_temperature) {
    BabyCare::Test::InMemoryDataStorage *dataStorage = new BabyCare::Test::InMemoryDataStorage();
    Core::SensorDataRepository sensorDataRepository(dataStorage);

    std::map<uint16_t, uint16_t> sensorData = sensorDataRepository.getSensorData(Type::SensorType::temperature);

    ASSERT_EQ(sensorData.size(), 2);
    ASSERT_EQ(sensorData[0], 512);
    ASSERT_EQ(sensorData[1], 768);
}

TEST(SensorDataRepositoryTest, it_can_register_sensor_data_for_heart_beat) {
    BabyCare::Test::InMemoryDataStorage *dataStorage = new BabyCare::Test::InMemoryDataStorage();
    Core::SensorDataRepository sensorDataRepository(dataStorage);

    Type::SensorType sensorToRegister = Type::SensorType::heartbeat;

    Type::ErrorType error = sensorDataRepository.registerSensorData(sensorToRegister, 1023);

    ASSERT_EQ(error, Type::ErrorType::ok);

    std::map<uint16_t, uint16_t> sensorData = sensorDataRepository.getSensorData(sensorToRegister);

    ASSERT_EQ(sensorData.size(), 3);
    ASSERT_EQ(sensorData[0], 256);
    ASSERT_EQ(sensorData[1], 512);
    ASSERT_EQ(sensorData[2], 1023);
}

TEST(SensorDataRepositoryTest, it_can_register_sensor_data_for_temperature) {
    BabyCare::Test::InMemoryDataStorage *dataStorage = new BabyCare::Test::InMemoryDataStorage();
    Core::SensorDataRepository sensorDataRepository(dataStorage);

    Type::SensorType sensorToRegister = Type::SensorType::temperature;

    Type::ErrorType error = sensorDataRepository.registerSensorData(sensorToRegister, 1023);

    ASSERT_EQ(error, Type::ErrorType::ok);

    std::map<uint16_t, uint16_t> sensorData = sensorDataRepository.getSensorData(sensorToRegister);

    ASSERT_EQ(sensorData.size(), 3);
    ASSERT_EQ(sensorData[0], 512);
    ASSERT_EQ(sensorData[1], 768);
    ASSERT_EQ(sensorData[2], 1023);
}

TEST(SensorDataRepositoryTest, it_can_register_sensor_data_for_microphone) {
    BabyCare::Test::InMemoryDataStorage *dataStorage = new BabyCare::Test::InMemoryDataStorage();
    Core::SensorDataRepository sensorDataRepository(dataStorage);

    Type::SensorType sensorToRegister = Type::SensorType::microphone;

    Type::ErrorType error = sensorDataRepository.registerSensorData(sensorToRegister, 1023);

    ASSERT_EQ(error, Type::ErrorType::ok);

    std::map<uint16_t, uint16_t> sensorData = sensorDataRepository.getSensorData(sensorToRegister);

    ASSERT_EQ(sensorData.size(), 2);
    ASSERT_EQ(sensorData[0], 768);
    ASSERT_EQ(sensorData[1], 1023);
}