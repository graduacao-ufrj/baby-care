#include "../core/ChecksumCalculator.h"

#include <gtest/gtest.h>

using namespace BabyCare;
TEST(ChecksumCalculatorTest, test_it_calculates_checksum_correctly_from_data_and_modulo) {
    uint16_t modulo = 4;
    
    uint16_t data = 0b0010000000000000;
    uint16_t checksum = Core::ChecksumCalculator::calculate(data, modulo);
    EXPECT_EQ(checksum, 1);

    data = 0b0010000000000001;
    checksum = Core::ChecksumCalculator::calculate(data, modulo);
    EXPECT_EQ(checksum, 2);

    data = 0b0010000000000011;
    checksum = Core::ChecksumCalculator::calculate(data, modulo);
    EXPECT_EQ(checksum, 3);

    data = 0b0010000000000111;
    checksum = Core::ChecksumCalculator::calculate(data, modulo);
    EXPECT_EQ(checksum, 0);
}

TEST(ChecksumCalculatorTest, test_it_calculates_checksum_correctly_from_data_sensor_type_and_modulo) {
    uint16_t modulo = 4;
    
    Type::SensorType sensorType = Type::SensorType::temperature; // Adds 1 more '1'

    uint16_t data = 0b000000000000;
    uint16_t checksum = Core::ChecksumCalculator::calculate(data, sensorType, modulo);
    EXPECT_EQ(checksum, 1);

    data = 0b000000000001;
    checksum = Core::ChecksumCalculator::calculate(data, sensorType, modulo);
    EXPECT_EQ(checksum, 2);

    data = 0b000000000011;
    checksum = Core::ChecksumCalculator::calculate(data, sensorType, modulo);
    EXPECT_EQ(checksum, 3);

    data = 0b000000000111;
    checksum = Core::ChecksumCalculator::calculate(data, sensorType, modulo);
    EXPECT_EQ(checksum, 0);

    sensorType = Type::SensorType::heartbeat; // Adds 0 more '1'

    data = 0b000000000000;
    checksum = Core::ChecksumCalculator::calculate(data, sensorType, modulo);
    EXPECT_EQ(checksum, 0);

    data = 0b000000000001;
    checksum = Core::ChecksumCalculator::calculate(data, sensorType, modulo);
    EXPECT_EQ(checksum, 1);

    data = 0b000000000011;
    checksum = Core::ChecksumCalculator::calculate(data, sensorType, modulo);
    EXPECT_EQ(checksum, 2);

    data = 0b000000000111;
    checksum = Core::ChecksumCalculator::calculate(data, sensorType, modulo);
    EXPECT_EQ(checksum, 3);

    sensorType = Type::SensorType::microphone; // Adds 1 more '1'

    data = 0b000000000000;
    checksum = Core::ChecksumCalculator::calculate(data, sensorType, modulo);
    EXPECT_EQ(checksum, 1);

    data = 0b000000000001;
    checksum = Core::ChecksumCalculator::calculate(data, sensorType, modulo);
    EXPECT_EQ(checksum, 2);

    data = 0b000000000011;
    checksum = Core::ChecksumCalculator::calculate(data, sensorType, modulo);
    EXPECT_EQ(checksum, 3);

    data = 0b000000000111;
    checksum = Core::ChecksumCalculator::calculate(data, sensorType, modulo);
    EXPECT_EQ(checksum, 0);
}