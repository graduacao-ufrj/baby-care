#include "../core/SensorTypeDemux.h"

#include "../core/Types.h"

#include <map>
#include <cstdint>

#include <gtest/gtest.h>

#ifdef DEBUG
    #include <iostream>
    #define DEBUG_PRINT(x) std::cout << x << std::endl;
#else
    #define DEBUG_PRINT(x)
#endif

using namespace BabyCare;
TEST(SensorTypeDemuxTest, test_it_returns_correct_sensor_and_data)
{
    Type::SensorType sensorType = Type::SensorType::heartbeat;
    DEBUG_PRINT("[TEST] Sensor type (binary): " << std::bitset<4>(static_cast<uint8_t>(sensorType)));

    uint16_t data = 564;
    DEBUG_PRINT("[TEST] Data (binary): " << std::bitset<10>(data));

    uint8_t checksum = 0;

    // Calculate the checksum
    uint16_t constructedData = (static_cast<uint16_t>(sensorType) << 10) | data;
    for (uint8_t i = 0; i < 14; i++)
    {
        checksum += (constructedData >> i) & 0x01;
    }
    DEBUG_PRINT("[TEST] Checksum (binary): " << std::bitset<8>(checksum));

    // Calculate the validation
    uint8_t validation = checksum % 4;
    DEBUG_PRINT("[TEST] Validation (binary): " << std::bitset<2>(validation));

    // Construct the data
    // Data: [ b1 b2 ] [ b3 b4 b5 b6 ] [ b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 ]
    constructedData = ((validation << 14) & 0xC000 ) | constructedData;

    DEBUG_PRINT("[TEST] Constructed data (binary): " << std::bitset<16>(constructedData));

    // Demux the data
    DEBUG_PRINT("[TARGET CODE EXECUTION] SensorTypeDemux::demux");
    std::map<Type::SensorType, uint16_t> result = Core::SensorTypeDemux::demux(constructedData);
    DEBUG_PRINT("[TARGET CODE EXECUTION ENDED]");

    // Check if the sensor type and data are correct
    // ASSERT_TRUE(result.find(sensorType) != result.end());
    ASSERT_EQ(result[sensorType], data);
}

TEST(SensorTypeDemuxTest, test_it_throws_exception_on_invalid_checksum)
{
    Type::SensorType sensorType = Type::SensorType::sensorAmount;
    uint16_t data = 564;

    uint16_t constructedData = (static_cast<uint16_t>(sensorType) << 10) | data;
    uint8_t validation = 3;

    constructedData = ((validation << 14) & 0xC000 ) | constructedData;
    ASSERT_THROW(Core::SensorTypeDemux::demux(constructedData), std::runtime_error);
}

TEST(SensorTypeDemuxTest, test_it_throws_exception_on_invalid_sensor_type)
{
    Type::SensorType sensorType = Type::SensorType::sensorAmount;
    uint16_t data = 564;
    uint8_t checksum = 0;

    uint16_t constructedData = (static_cast<uint16_t>(sensorType) << 10) | data;
    for (uint8_t i = 0; i < 14; i++)
    {
        checksum += (constructedData >> i) & 0x01;
    }
    uint8_t validation = checksum % 4;

    constructedData = ((validation << 14) & 0xC000 ) | constructedData;
    ASSERT_THROW(Core::SensorTypeDemux::demux(constructedData), std::runtime_error);
}