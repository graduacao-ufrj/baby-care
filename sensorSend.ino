#include <Wire.h>
#include <stdio.h>
#include <stdlib.h>

#define POT_PORT A3
#define FOUR_BIT_PORT_1 5
#define FOUR_BIT_PORT_2 4
#define FOUR_BIT_PORT_3 3
#define FOUR_BIT_PORT_4 2

unsigned int sensorRead = 0;
unsigned char address = 6;

void setup()
{
    pinMode(POT_PORT, INPUT);
    pinMode(FOUR_BIT_PORT_1, INPUT);
    pinMode(FOUR_BIT_PORT_2, INPUT);
    pinMode(FOUR_BIT_PORT_3, INPUT);
    pinMode(FOUR_BIT_PORT_4, INPUT);

    delay(50);

    address = (digitalRead(FOUR_BIT_PORT_1) << 3) | (digitalRead(FOUR_BIT_PORT_2) << 2) | (digitalRead(FOUR_BIT_PORT_3) << 1) | digitalRead(FOUR_BIT_PORT_4);
    Wire.begin(address);

    Serial.begin(9600);

    if (address == 0) {
        Serial.println("Address is 0, setting to 6");
        address = 6;
    }

    if (Serial) {
        Serial.print("Sensor address: ");
        Serial.println(address);
    }

    Wire.onRequest(sendData);
}

void loop()
{
    sensorRead = analogRead(POT_PORT);
    delay(10);
}

void sendData()
{
    unsigned char *bytes = (unsigned char*)malloc(sizeof(unsigned char) * 2);
    convertSensorValue(sensorRead, bytes);
    writeData(bytes);
}

// Function to convert sensor value into two bytes
void convertSensorValue(unsigned int sensorRead, unsigned char* bytes)
{
    unsigned int numBytes = sizeof(bytes);
    unsigned int accumulator = sensorRead;

    for (int i = 0; i < numBytes; i++) {
        unsigned int toSend = accumulator;
        if (i != numBytes - 1) {
            toSend /= (256 * (numBytes - i - 1));
        }
        accumulator -= toSend * 256 * (numBytes - i - 1);

        bytes[i] = (unsigned char) toSend;
    }
}

// Function to write data to the wire
void writeData(unsigned char *bytes)
{
    Serial.println(sensorRead);
    for (int i = 0; i < sizeof(bytes); i++) {
        Wire.write(bytes[i]);
    }
}
